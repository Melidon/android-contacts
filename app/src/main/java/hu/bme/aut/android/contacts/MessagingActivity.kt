package hu.bme.aut.android.contacts

import android.Manifest
import android.os.Bundle
import android.telephony.SmsManager
import android.view.LayoutInflater
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import hu.bme.aut.android.contacts.databinding.ActivityMessagingBinding
import hu.bme.aut.android.contacts.model.Contact
import permissions.dispatcher.*

@RuntimePermissions
class MessagingActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMessagingBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMessagingBinding.inflate(LayoutInflater.from(this))
        setContentView(binding.root)

        binding.contactName.text = intent.getStringExtra(Contact.KEY_NAME)
            ?: resources.getString(R.string.contact_name_placeholder)
        binding.contactNumber.text = intent.getStringExtra(Contact.KEY_NUMBER)
            ?: resources.getString(R.string.contact_name_placeholder)

        binding.buttonSend.setOnClickListener {
            sendSmsWithPermissionCheck(
                binding.contactNumber.text.toString(),
                binding.smsBody.text.toString()
            )
        }

    }

    @NeedsPermission(Manifest.permission.SEND_SMS)
    fun sendSms(phoneNumber: String, message: String) {
        try {
            // Ez csak simán megnyitja az alkalmazást
            /*
            val sendSmsIntent = Intent(Intent.ACTION_VIEW)
            sendSmsIntent.data = Uri.parse("smsto:")
            sendSmsIntent.type = "vnd.android-dir/mms-sms"
            sendSmsIntent.putExtra("address", phoneNumber)
            sendSmsIntent.putExtra("sms_body", message)
            startActivity(sendSmsIntent)
            */

            // Ez kapásból elküldi
            val smgr: SmsManager = SmsManager.getDefault()
            smgr.sendTextMessage(phoneNumber, null, message, null, null)

            binding.smsBody.text.clear()
            Toast.makeText(
                this,
                getString(R.string.sms_sent_successfully),
                Toast.LENGTH_SHORT
            ).show()
        } catch (e: Exception) {
            Toast.makeText(
                this,
                getString(R.string.failed_to_send_sms),
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    @OnPermissionDenied(Manifest.permission.SEND_SMS)
    fun onSendSmsDenied() {
        Toast.makeText(this, getString(R.string.permission_denied_send_sms), Toast.LENGTH_SHORT).show()
    }

    @OnShowRationale(Manifest.permission.SEND_SMS)
    fun showRationaleForSendSms(request: PermissionRequest) {
        val alertDialog = AlertDialog.Builder(this)
            .setTitle(title)
            .setMessage(R.string.send_sms_permission_explanation)
            .setCancelable(false)
            .setPositiveButton(R.string.proceed) { dialog, id -> request.proceed() }
            .setNegativeButton(R.string.exit) { dialog, id -> request.cancel() }
            .create()
        alertDialog.show()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        // NOTE: delegate the permission handling to generated method
        onRequestPermissionsResult(requestCode, grantResults)
    }
}