package hu.bme.aut.android.contacts.util

import android.database.Cursor

fun Cursor.getStringByColumnName(colName: String) = this.getString(this.getColumnIndex(colName))